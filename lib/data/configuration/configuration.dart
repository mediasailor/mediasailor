import 'dart:io';

/// Aplication configuration properties.
///
/// * **author**: iruzo
/// * **params**: void.
/// * **return**: void.
class Configuration {
  /// e.g. /home/[user]
  String? get homeUserPath =>
      Platform.environment['HOME'] ?? Platform.environment['USERPROFILE'];

  /// e.g. 'pwd'/.msailor
  String get msailorUserPath => _msailorUserPath();

  /// e.g. 'pwd'/.msailor/log
  String logPath({String? value}) =>
      _configFile('logPath', msailorUserPath + Platform.pathSeparator + 'log',
          overrideValue: value);

  /// e.g. 'pwd'/.msailor/history
  String historyPath({String? value}) => _configFile(
      'historyPath', msailorUserPath + Platform.pathSeparator + 'history',
      overrideValue: value);

  /// e.g. 'pwd'/.msailor/history/searchHistory
  String searchHistoryPath({String? value}) =>
      historyPath() + Platform.pathSeparator + 'searchHistory';

  /// e.g. 'pwd'/.msailor/history/downloadHistory
  String downloadHistoryPath({String? value}) =>
      historyPath() + Platform.pathSeparator + 'downloadHistory';

  /// e.g. 'pwd'/.msailor/library
  String libraryPath({String? value}) => _configFile(
      'libraryPath', msailorUserPath + Platform.pathSeparator + 'library',
      overrideValue: value);

  void resetPropertiesFile() {
    File file = File("$msailorUserPath/client.properties");
    if (file.existsSync()) {
      file.deleteSync();
    }
    this._createPropertiesFile(file);
  }

  void _createPropertiesFile(File file) {
    file.createSync();
    historyPath();
    libraryPath();
    logPath();
  }

  /// Create file if it does not exist and set default parameters if they are not overwritten.
  ///
  /// * **author**: iruzo
  /// * **params**:
  ///   * key (name of the configuration property)
  ///   * defaultValue (default value of the property)
  ///   * overrideValue.(optional value to override configFile value, use 'default' to restore the value)
  /// * **return**: value of the property.
  String _configFile(String key, String defaultValue, {String? overrideValue}) {
    File file = File("$msailorUserPath/client.properties");
    if (!file.existsSync()) _createPropertiesFile(file);
    if (overrideValue == null) {
      List<String> lines = file.readAsLinesSync();
      for (String line in lines) {
        if (line.contains("$key")) {
          return line.split("=")[1];
        }
      }
      file.writeAsStringSync('$key=$defaultValue' + '\n',
          mode: FileMode.append);
      return defaultValue;
    } else {
      List<String> lines = file.readAsLinesSync();
      for (int ii = lines.length - 1; ii >= 0; ii--) {
        if (lines[ii].contains('$key')) {
          lines.removeAt(ii);
        }
      }
      lines.add(overrideValue == 'default'
          ? '$key=$defaultValue'
          : '$key=$overrideValue');
      file.writeAsStringSync(lines.join('\n'));
      return overrideValue;
    }
  }

  String _msailorUserPath() {
    Directory msailorUserDirectory =
        Directory('$homeUserPath' + Platform.pathSeparator + '.msailor');
    if (!msailorUserDirectory.existsSync()) msailorUserDirectory.createSync();
    return '$homeUserPath' + Platform.pathSeparator + '.msailor';
  }
}
