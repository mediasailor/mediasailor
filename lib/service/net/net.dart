import 'package:msailor/service/net/job/web_scrapping.dart';

/// Network functionalities
///
/// * **author**: iruzo
/// * **params**: void.
/// * **return**: void.
class Net {
  /// Network functionalities
  ///
  /// * **author**: iruzo
  /// * **params**: void.
  /// * **return**: instance.
  Net();

  /// Scrap a web page.
  ///
  /// * **author**: iruzo
  /// * **params**: URL and path to the desire label (e.g. h3.title > a.caption | or just html, head or body).
  /// * **return**: All the web elements as JSON.
  /// * **WARNING**: If the web contains javascript to load something dinamically, the returned JSON will probably contain it too and the JSON structure could be incomplete or wrong.
  Future<String> webScrapping(String url, String label) =>
      WebScrapping(url, label).call();
}
