import 'package:msailor/service/service.dart';
import 'package:msailor/data/data.dart';

/// Access to the multiple parts of the application.
///
/// * **author**: iruzo
/// * **params**: void.
/// * **return**: void.
abstract class Calls {
  /// Manage the calls to the functionalities of the logical part of the application.
  ///
  /// * **author**: iruzo
  /// * **params**: void.
  /// * **return**: instance.
  Service service = Service();

  /// Manage all storagement types in the application.
  ///
  /// * **author**: iruzo
  /// * **params**: void.
  /// * **return**: instance.
  Data data = Data();
}
