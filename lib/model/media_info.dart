import 'dart:convert';

/// Store data from .
///
/// * **author**: iruzo
/// * **params**:
///   * String source (youtube, ftp ...)
///   * String mediaId,
///   * String url
///   * bool live
///   * String channelName
///   * String channelSubDirUrl (/usr/... | /channel/...)
///   * String channelAvatarUrl
///   * String title
///   * String videoPicUrl
///   * String length
///   * String views
/// * **return**: void.
class MediaInfo {
  /// Store data from .
  ///
  /// * **author**: iruzo
  /// * **params**:
  ///   * String source (youtube, ftp ...)
  ///   * String mediaId,
  ///   * String url
  ///   * bool live
  ///   * String channelName
  ///   * String channelSubDirUrl (/usr/... | /channel/...)
  ///   * String channelAvatarUrl
  ///   * String title
  ///   * String videoPicUrl
  ///   * String length
  ///   * String views
  /// * **return**: instance.
  MediaInfo(
      {this.source,
      this.id,
      this.url,
      this.live,
      this.channelName,
      this.channelSubDirUrl,
      this.channelAvatarUrl,
      this.title,
      this.videoPicUrl,
      this.length,
      this.views});

  /// Store data from .
  ///
  /// * **author**: iruzo
  /// * **params**: json
  /// * **return**: instance.
  factory MediaInfo.fromJson(Map<String, dynamic> json) {
    return MediaInfo(
        source: json['source'] as String,
        id: json['id'] as String,
        url: json['url'] as String,
        live: json['live'] as bool,
        channelName: json['channelName'] as String,
        channelSubDirUrl: json['channelSubDirUrl'] as String,
        channelAvatarUrl: json['channelAvatarUrl'] as String,
        title: json['title'] as String,
        videoPicUrl: json['videoPicUrl'] as String,
        length: json['length'] as String,
        views: json['views'] as String);
  }

  String? source;
  String? id;
  String? url;
  bool? live;
  String? channelName;
  String? channelSubDirUrl;
  String? channelAvatarUrl;
  String? title;
  String? videoPicUrl;
  String? length;
  String? views;

  String toJson() => json.encode({
        'source': this.source,
        'id': this.id,
        'url': this.url,
        'live': this.live,
        'channelName': this.channelName,
        'channelSubDirUrl': this.channelSubDirUrl,
        'channelAvatarUrl': this.channelAvatarUrl,
        'title': this.title,
        'videoPicUrl': this.videoPicUrl,
        'length': this.length,
        'views': this.views
      });
}
