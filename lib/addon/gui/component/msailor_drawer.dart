import 'dart:async';

import 'package:flutter/material.dart';
import 'package:msailor/addon/gui/component/body-component/history/msailor_download_history.dart';
import 'package:msailor/addon/gui/component/body-component/history/msailor_search_history.dart';
import 'package:msailor/addon/gui/component/body-component/msailor_settings.dart';
import 'package:msailor/data/data.dart';

class MsailorDrawer extends StatelessWidget {
  MsailorDrawer(this.streamController);

  final StreamController<Widget> streamController;

  String _screenOnStream = '';

  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: ListView(
      padding: EdgeInsets.zero,
      children: [
        ListTile(
          leading: Icon(Icons.file_download),
          title: Text("Downloads"),
          onTap: () {
            if (this._screenOnStream != 'download') {
              this._screenOnStream = 'download';
              streamController.add(MsailorDownloadHistory(
                  Data().configuration.downloadHistoryPath()));
              Navigator.pop(context);
            }
          },
        ),
        ListTile(
          leading: Icon(Icons.history),
          title: Text("History"),
          onTap: () {
            if (this._screenOnStream != 'history') {
              this._screenOnStream = 'history';
              streamController.add(
                  MsailorSearchHistory(Data().configuration.searchHistoryPath()));
              Navigator.pop(context);
            }
          },
        ),
        Center(child: Text(("\n\n"))),
        ListTile(
          leading: Icon(Icons.star),
          title: Text("Favorite Albums"),
        ),
        ListTile(
          leading: Icon(Icons.favorite),
          title: Text("Favorite Tracks"),
        ),
        ListTile(
          leading: Icon(Icons.file_present),
          title: Text("Local Library"),
        ),
        Center(child: Text(("\n\n"))),
        ListTile(
          leading: Icon(Icons.settings),
          title: Text("Settings"),
          onTap: () {
            if (this._screenOnStream != 'history') {
              this._screenOnStream = 'history';
              streamController.add(MsailorSettings());
              Navigator.pop(context);
            }
          },
        ),
      ],
    ));
  }
}
